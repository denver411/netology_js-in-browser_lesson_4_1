'use strict';

function createContent(data) {
  document.querySelector('[data-pic]').style = `background-image: url(${data.pic})`;
  document.querySelector('[data-title]').innerText = data.title;
  document.querySelector('[data-ingredients]').innerText = data.ingredients.join(', ');
}

function createRating(data) {
  document.querySelector('[data-rating]').innerText = data.rating.toFixed(2);
  document.querySelector('[data-star]').style = `width: ${data.rating/10*100}%`;
  document.querySelector('[data-votes]').innerText = `(${data.votes} оценок)`;
}

function createConsumers(data) {
  data.consumers.forEach(el => {
    document.querySelector('[data-consumers]').appendChild(document.createElement('img')).outerHTML = `<img src="${el.pic}" title="${el.name}">`;
  })
  document.querySelector('[data-consumers]').appendChild(document.createElement('span')).innerText = `(+${data.total})`;
}

function loadData(url, callback) {
  return new Promise((done, fail) => {
    window[callback] = done;
    const script = document.createElement('script');
    script.src = `${url}?jsonp=${callback}`;
    document.body.appendChild(script);
  });
}

loadData('https://neto-api.herokuapp.com/food/42', 'receiptData')
  .then(createContent)

loadData('https://neto-api.herokuapp.com/food/42/rating', 'receiptRating')
  .then(createRating)

loadData('https://neto-api.herokuapp.com/food/42/consumers', 'receiptConsumers')
  .then(createConsumers)