'use strict';

//data-following похоже в описании задачи лишний

function createContent(data) {
  console.log(data);
  document.querySelector('[data-name]').innerText = data.name;
  document.querySelector('[data-description]').innerText = data.description;
  document.querySelector('[data-pic]').src = data.pic;
  document.querySelector('[data-position]').innerText = data.position;
  const techsNode = document.querySelector('[data-technologies]');
  loadData(`https://neto-api.herokuapp.com/profile/${data.id}/technologies`, 'loadTechnologies')
    .then(techs => {
      techs.forEach(element => {
        const tagTech = document.createElement('span');
        tagTech.classList.add('devicons', `devicons-${element}`)
        techsNode.appendChild(tagTech);
      });
    })
}

function loadData(url, funcName) {
  return new Promise((done, fail) => {
    window[funcName] = done;
    const script = document.createElement('script');
    script.src = `${url}?jsonp=${funcName}`;
    document.body.appendChild(script);
  });
}

loadData('https://neto-api.herokuapp.com/profile/me', 'loadUser')
  .then(createContent)
  .then(document.querySelector('.content').style.display = 'initial')