'use strict';

function createContent(data) {
  console.log(data);
  document.querySelector('[data-wallpaper]').src = data.wallpaper;
  document.querySelector('[data-username]').innerText = data.username;
  document.querySelector('[data-description]').innerText = data.description;
  document.querySelector('[data-pic]').src = data.pic;
  document.querySelector('[data-tweets]').innerText = data.tweets;
  document.querySelector('[data-followers]').innerText = data.followers;
  document.querySelector('[data-following]').innerText = data.following;
}

function loadData(url) {
  return new Promise((done, fail) => {
    window.functionName = done;
    const script = document.createElement('script');
    script.src = `${url}?jsonp=functionName`;
    document.body.appendChild(script);
  });
}

loadData('https://neto-api.herokuapp.com/twitter/jsonp')
  .then(createContent)